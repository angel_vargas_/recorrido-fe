# Recorrido CL

This project is the frontend part of an interview application when I was
applying to recorrido.cl

# Installation

You'll need:

- NodeJS 8.x

```shell
yarn install
```

# Running the app

```shell
yarn start
```
