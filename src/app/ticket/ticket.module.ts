import { NgModule } from '@angular/core';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule,
  MatInputModule,
  MatSelectModule
} from '@angular/material';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { FormComponent } from './form.component';

@NgModule({
  imports: [
    MatInputModule,
    MatSelectModule,
    MatCardModule,
    CommonModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    MatButtonModule
  ],
  declarations: [FormComponent],
  exports: [FormComponent]
})
export class TickerModule { }
