import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';

import { cityValidator } from './ticket.validator';
import { TicketService } from '../shared/ticket.service';
import { City } from '../shared/city';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-ticket-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  @Input() cities: City[];
  @Output() creation = new EventEmitter();
  @ViewChild('form') formElement;

  filteredOriginCities: Observable<City[]>;
  filteredDestinationCities: Observable<City[]>;

  constructor(
    private fb: FormBuilder,
    private ticketService: TicketService
  ) { }

  ticketForm = this.fb.group({
    origin: ['', cityValidator()],
    destination: [''],
    price: ['']
  });

  ngOnInit() {
    this.filteredOriginCities = this.ticketForm.controls['origin'].valueChanges
      .pipe(
        startWith<string | City>(''),
        map(value => typeof value === 'string' ? value : value.name),
        map(name => name ? this._filter(name) : this.cities.slice())
      );

    this.filteredDestinationCities = this.ticketForm.controls['destination'].valueChanges
      .pipe(
        startWith<string | City>(''),
        map(value => typeof value === 'string' ? value : value.name),
        map(name => name ? this._filter(name) : this.cities.slice())
      );
  }

  displayCity(city: City): string | null {
    return city ? city.name : null;
  }

  onSubmit() {
    if (this.ticketForm.invalid) { return; }

    this.ticketService.createTicket(this.ticketForm.getRawValue()).subscribe(() => {
      this.formElement.resetForm();
    });
  }

  private _filter(name: string): City[] {
    const filterValue = name.toLowerCase();

    return this.cities.filter(city => city.name.toLowerCase().indexOf(filterValue) === 0);
  }
}
