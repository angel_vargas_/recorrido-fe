import { AbstractControl, ValidatorFn } from '@angular/forms';

export function cityValidator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const value = control.value;
    return value && value.id && value.name ? null : { city: { value: control.value } };
  };
}
