import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-ticket-statistics',
  templateUrl: './statistics.component.html'
})
export class StatisticsComponent {
  @Input() totalIncome: number;
  @Input() totalSales: number;
}
