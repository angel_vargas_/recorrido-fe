import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material';
import { CommonModule } from '@angular/common';

import { StatisticsComponent } from './statistics.component';

@NgModule({
  imports: [MatCardModule, CommonModule],
  declarations: [StatisticsComponent],
  exports: [StatisticsComponent]
})
export class StatisticsModule { }
