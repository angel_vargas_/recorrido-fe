import { Component, OnInit } from '@angular/core';

import { TicketService } from './shared/ticket.service';
import { CityService } from './shared/city.service';
import { City } from './shared/city';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  cities: City[] = [];
  statistics: any;

  constructor(
    private ticketService: TicketService,
    private cityService: CityService
  ) { }

  ngOnInit() {
    this.ticketService.subcribeToChannel();
    this.ticketService.fetchStatistics();
    this.cityService.fetchAll().subscribe((cities: City[]) => {
      this.cities = cities;
    });
    this.ticketService.onUpdateStatistics.subscribe((statistics) => {
      this.statistics = statistics;
    });
  }

  createTicket(ticket) {
    this.ticketService.createTicket(ticket);
  }
}
