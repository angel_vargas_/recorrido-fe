import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { MapModule } from './map/map.module';
import { TicketService } from './shared/ticket.service';
import { TickerModule } from './ticket/ticket.module';
import { CityService } from './shared/city.service';
import { StatisticsModule } from './ticket/statistics/statistics.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    MapModule,
    BrowserAnimationsModule,
    TickerModule,
    CommonModule,
    HttpClientModule,
    StatisticsModule
  ],
  providers: [
    TicketService,
    CityService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
