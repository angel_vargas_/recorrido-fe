import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject, timer } from 'rxjs';
import * as ActionCable from 'actioncable';

import { environment } from 'src/environments/environment';
import { Ticket } from '../shared/ticket';

@Injectable()
export class TicketService {
  onAddTicket = new Subject<Ticket>();
  onRemoveTicket = new Subject<Ticket>();
  onUpdateStatistics = new Subject<Ticket>();

  constructor(
    private http: HttpClient
  ) { }

  subcribeToChannel() {
    const cable = ActionCable.createConsumer(environment.CABLE_URL);
    cable.subscriptions.create('TicketChannel', {
      received: ({ action, record}) => {
        if (action === 'created') {
          this.addTicket(record);
          timer(10000).subscribe(() => {
            this.removeTicket(record);
          });
        }

        if (action === 'statistics') {
          this.updateStatistics(record);
        }
      }
    });
  }

  updateStatistics(statistics) {
    this.onUpdateStatistics.next(statistics);
  }

  addTicket(ticket: Ticket) {
    this.onAddTicket.next(ticket);
  }

  removeTicket(ticket: Ticket) {
    this.onRemoveTicket.next(ticket);
  }

  createTicket(ticket: Ticket) {
    return this.http.post(`${environment.API}/v1/tickets`, {
      ticket: {
        origin_id: ticket.origin.id,
        destination_id: ticket.destination.id,
        price: ticket.price
      }
    });
  }

  fetchStatistics() {
    return this.http.get(`${environment.API}/v1/tickets_statistics`).subscribe(statistics => {
      this.updateStatistics(statistics);
    });
  }
}
