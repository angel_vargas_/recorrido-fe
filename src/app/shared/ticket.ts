import { City } from './city';

export interface Ticket {
  id: number;
  price: number;
  origin: City;
  destination: City;
}
