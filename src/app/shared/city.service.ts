import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment';

@Injectable()
export class CityService {
  constructor(private http: HttpClient) { }

  fetchAll() {
    return this.http.get(`${environment.API}/v1/cities`);
  }
}
