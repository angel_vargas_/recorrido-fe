import { ElementRef, Input, OnInit, Directive } from '@angular/core';
import * as L from 'leaflet';
import 'leaflet-routing-machine';

import { TicketService } from '../shared/ticket.service';
import { Ticket } from '../shared/ticket';

@Directive({
  selector: '[appMap]'
})
export class MapDirective implements OnInit {
  @Input() tickets: Ticket[];
  map: L.Map;
  routes: any = {};

  constructor(
    private ticketService: TicketService,
    private mapContainer: ElementRef
  ) { }

  ngOnInit() {
    this.map = L.map(this.mapContainer.nativeElement).setView([-40, -20], 4);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?').addTo(this.map);

    this.ticketService.onAddTicket.subscribe((ticket) => {
      this.addRoute(ticket);
    });

    this.ticketService.onRemoveTicket.subscribe((ticket) => {
      this.removeRoute(ticket);
    });
  }

  addRoute({ id, origin, destination }) {
    const waypoints = [
      L.latLng(origin.latitude, origin.longitude),
      L.latLng(destination.latitude, destination.longitude)
    ];

    this.routes[id] = L.Routing.control({
      fitSelectedRoutes: false,
      plan: L.Routing.plan(waypoints, {
        createMarker: (i, wp) => (
          L.marker(wp.latLng, {
            icon: L.divIcon({
              html: `<span>${String.fromCharCode(65 + i)}</span>`,
              iconSize: [ 25, 41 ],
              iconAnchor: [ 13, 41 ],
              className: 'icon-container'
            })
          })
        )
      })
    });

    this.routes[id].addTo(this.map);
  }

  removeRoute({ id }) {
    const route = this.routes[id];
    if (route) {
      this.map.removeControl(route);
    }
  }
}
