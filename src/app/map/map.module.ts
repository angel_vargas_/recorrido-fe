import { NgModule } from '@angular/core';

import { MapDirective } from './map.directive';

@NgModule({
  declarations: [MapDirective],
  exports: [MapDirective]
})
export class MapModule { }
