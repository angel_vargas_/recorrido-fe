export const environment = {
  PRODUCTION: true,
  API: 'http://localhost:3000',
  CABLE_URL: 'ws://localhost:3000/cable'
};
